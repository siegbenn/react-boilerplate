# README #

Here's a boilerplate webapp I started working on. It uses the React ecosystem, AWS Cognito for user authentication and tachyons for CSS. 

* Landing page.
* User registration, login, confirmation and password reset.

### How do I get set up? ###

* Clone the repo.
* $ npm install
* $ npm start