import { updatePassword } from '../helpers/user';
import { addTimedNotification } from './notifications';
import { REHYDRATE } from 'redux-persist/constants'

const FETCHING_UPDATE_PASSWORD = 'FETCHING_UPDATE_PASSWORD'
const UPDATE_PASSWORD_SUCCESS = 'UPDATE_PASSWORD_SUCCESS'
const UPDATE_PASSWORD_FAILURE = 'UPDATE_PASSWORD_FAILURE'


function fetchingUpdatePassword() {
    return {
        type: FETCHING_UPDATE_PASSWORD
    }
}

function updatePasswordSuccess() {
    return {
        type: UPDATE_PASSWORD_SUCCESS
    }
}

function updatePasswordFailure(error) {
    return {
        type: UPDATE_PASSWORD_FAILURE,
        error
    }
}

const initialState = {
    isFetchingUpdatePassword: false,
}

export default function user(state = initialState, action) {
    switch (action.type) {
        case REHYDRATE:
            let incoming = action.payload.user
            if (incoming) return {...state, ...incoming }
            return state
        case FETCHING_UPDATE_PASSWORD:
            return {
                ...state,
                error: '',
                isFetchingUpdatePassword: true
            }
        case UPDATE_PASSWORD_FAILURE:
            return {
                ...state,
                isFetchingUpdatePassword: false,
                error: action.error
            }
        case UPDATE_PASSWORD_SUCCESS:
            return {
                ...state,
                isFetching: false,
                error: ''
            }
        default:
            return state
    }
}

export function updateUserPassword(password) {
    return function(dispatch) {
        dispatch(fetchingUpdatePassword())
        return updatePassword(password)
            .then((response) => {
                dispatch(updatePasswordSuccess())
                dispatch(addTimedNotification('green', 'Password changed successfully.'))
            })
            .catch((error) => {
                dispatch(updatePasswordFailure(error.message))
                dispatch(addTimedNotification('light-red', error.message))
            })
    }
}
