import { signUp, verify, recover, resetPassword } from '../helpers/auth';
import { addTimedNotification } from './notifications';
import { change, untouch, destroy } from 'redux-form';
import { browserHistory } from 'react-router'
import { REHYDRATE } from 'redux-persist/constants'



const FETCHING_REGISTRATION = 'FETCHING_REGISTRATION';
const REGISTRATION_ERROR = 'REGISTRATION_ERROR';
const REGISTRATION_SUCCESS = 'REGISTRATION_SUCCESS';

const FETCHING_VERIFICATION = 'FETCHING_VERIFICATION';
const VERIFICATION_SUCCESS = 'VERIFICATION_SUCCESS';
const VERIFICATION_ERROR = 'VERIFICATION_ERROR';

const FETCHING_RECOVER = 'FETCHING_RECOVER';
const RECOVER_ERROR = 'RECOVER_ERROR';
const RECOVER_SUCCESS = 'RECOVER_SUCCESS';

const FETCHING_RESET_PASSWORD = 'FETCHING_RESET_PASSWORD';
const RESET_PASSWORD_ERROR = 'RESET_PASSWORD_ERROR';
const RESET_PASSWORD_SUCCESS = 'RESET_PASSWORD_SUCCESS';

function fetchingRegistration() {
    return {
        type: FETCHING_REGISTRATION
    }
}

function registrationError(error) {
    return {
        type: REGISTRATION_ERROR,
        error
    }
}

function registrationSuccess() {
    return {
        type: REGISTRATION_SUCCESS
    }
}

function fetchingVerification() {
    return {
        type: FETCHING_VERIFICATION
    }
}

function verificationError(error) {
    return {
        type: VERIFICATION_ERROR,
        error
    }
}

function verificationSuccess() {
    return {
        type: VERIFICATION_SUCCESS
    }
}

function fetchingRecover() {
    return {
        type: FETCHING_RECOVER
    }
}

function recoverError(error) {
    return {
        type: RECOVER_ERROR,
        error
    }
}

function recoverSuccess() {
    return {
        type: RECOVER_SUCCESS
    }
}

function fetchingResetPassword() {
    return {
        type: FETCHING_RESET_PASSWORD
    }
}

function resetPasswordError(error) {
    return {
        type: RESET_PASSWORD_ERROR,
        error
    }
}

function resetPasswordSuccess() {
    return {
        type: RESET_PASSWORD_SUCCESS
    }
}

const initialState = {
    isFetchingRegistration: false,
    isFetchingVerification: false,
    isFetchingRecover: false,
    isFetchingResetPassword: false,
    verificationError: '',
    registrationError: '',
    recoverError: '',
    resetPasswordError: ''
}

export default function registration(state = initialState, action) {
    switch (action.type) {
    	case REHYDRATE :
            let incoming = action.payload.registration
            if (incoming) return {...state, ...incoming}
            return state
    	case FETCHING_REGISTRATION:
    		return {
    			...state,
    			isFetchingRegistration: true
    		}
        case REGISTRATION_ERROR:
            return {
                ...state,
                isFetchingRegistration: false,
                registrationError: action.error
            }
        case REGISTRATION_SUCCESS:
            return {
                ...state,
                registrationError: '',
                isFetchingRegistration: false
            }
        case FETCHING_VERIFICATION:
    		return {
    			...state,
    			isFetchingVerification: true
    		}
        case VERIFICATION_ERROR:
            return {
                ...state,
                isFetchingVerification: false,
                verificationError: action.error
            }
        case VERIFICATION_SUCCESS:
            return {
                ...state,
                verificationError: '',
                isFetchingVerification: false
            }
        case FETCHING_RECOVER:
    		return {
    			...state,
    			isFetchingRecover: true
    		}
        case RECOVER_ERROR:
            return {
                ...state,
                isFetchingRecover: false,
                recoverError: action.error
            }
        case RECOVER_SUCCESS:
            return {
                ...state,
                recoverError: '',
                isFetchingRecover: false
            }
        case FETCHING_RESET_PASSWORD:
    		return {
    			...state,
    			isFetchingResetPassword: true
    		}
        case RESET_PASSWORD_ERROR:
            return {
                ...state,
                isFetchingResetPassword: false,
                resetPasswordError: action.error
            }
        case RESET_PASSWORD_SUCCESS:
            return {
                ...state,
                resetPasswordError: '',
                isFetchingResetPassword: false
            }
        default:
            return state
    }
}

export function signUpUser(username, email, password) {
    return function(dispatch) {
        dispatch(fetchingRegistration())
        return signUp(username, email, password)
            .then((response) => {
                dispatch(registrationSuccess())
                dispatch(untouch('register', 'password'))
                dispatch(change('register', 'password', ''))   
                dispatch(change('verify', 'username', username)) 
                dispatch(destroy('register'))  
                dispatch(addTimedNotification('green', 'You\'re almost done! We\'ve sent you a verification email. Please check your inbox.'))
                browserHistory.push('/verify')

            })
            .catch((error) => {
                dispatch(untouch('register', 'password'))
                dispatch(change('register', 'password', ''))
                dispatch(registrationError(error.message))

                if (error.message.toLowerCase().indexOf('password') !== -1) {
                dispatch(addTimedNotification('light-red', "Password must be at least 8 characters long."))
            	} else if (error.message[-1] !== ".") {
            		dispatch(addTimedNotification('light-red', error.message + "."))
            	} else {
            		dispatch(addTimedNotification('light-red', error.message))
            	}
            })
    }
}

export function verifyUser(username, code) {
    return function(dispatch) {
    	dispatch(fetchingVerification())
        return verify(username, code)
            .then((response) => {
            	dispatch(verificationSuccess())
            	dispatch(change('login', 'username', username)) 
            	dispatch(destroy('verify')) 
                dispatch(addTimedNotification('green', 'You\'ve been confirmed! You can now log in.'))
                browserHistory.push('/login')

            })
            .catch((error) => { 
            	dispatch(verificationError(error.message))
            	dispatch(addTimedNotification('light-red', error.message))
            })
    }
}

export function recoverUser (username) {
    return function(dispatch) {
    	dispatch(fetchingRecover())
        return recover(username)
            .then((response) => {
            	dispatch(recoverSuccess())
            	dispatch(change('resetPassword', 'username', username)) 
            	dispatch(destroy('recover')) 
                dispatch(addTimedNotification('green', 'Success! Check your inbox for a recovery email.'))
                browserHistory.push('/reset')

            })
            .catch((error) => { 
            	dispatch(recoverError(error.message))
            	dispatch(addTimedNotification('light-red', error.message))
            })
    }
}

export function resetUserPassword (username, code, password) {
    return function(dispatch) {
    	dispatch(fetchingResetPassword())
        return resetPassword(username, code, password)
            .then((response) => {
            	dispatch(resetPasswordSuccess())
            	dispatch(destroy('resetPassword')) 
                dispatch(addTimedNotification('green', 'Success! You can now log in using your new password.'))
                browserHistory.push('/login')

            })
            .catch((error) => { 
            	dispatch(untouch('resetPassword', 'password'))
                dispatch(change('resetPassword', 'password', ''))
                dispatch(resetPasswordError(error.message))
            	dispatch(addTimedNotification('light-red', error.message))
            })
    }
}
