import { OrderedMap, Map } from 'immutable';
import { getRandomInt } from '../helpers/utils';

const ADD_NOTIFICATION = "ADD_NOTIFICATION"
const REMOVE_NOTIFICATION = "REMOVE_NOTIFICATION"
const CLEAR_NOTIFICATIONS = "CLEAR_NOTIFICATIONS"

function addNotification(id, notificationType, text) {
    return {
        type: ADD_NOTIFICATION,
        notificationType,
        text,
        id
    }
}

export function removeNotification(id) {
    return {
        type: REMOVE_NOTIFICATION,
        id
    }
}

export function clearNotifications(id) {
    return {
        type: CLEAR_NOTIFICATIONS
    }
}

const initialState = Map({
    notifications: OrderedMap({})
})

export default function notifications (state = initialState, action) {
    switch (action.type) {
        case ADD_NOTIFICATION :
            return state.setIn(['notifications', action.id],
                    {
                        type: action.notificationType,
                        text: action.text
                    }
                )
        case REMOVE_NOTIFICATION :
            return state.deleteIn(['notifications', action.id])
        case CLEAR_NOTIFICATIONS :
            return Map({
                notifications: OrderedMap({})
            })
        default :
            return state
    }
}

export function addTimedNotification(notificationType, text) {

    const id = getRandomInt(1000,9999).toString()

    return function (dispatch) { 
        dispatch(addNotification(id, notificationType, text))
        setTimeout(() => {
            dispatch(removeNotification(id))
        }, 5000)
    }
}