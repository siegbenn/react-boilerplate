import { login, parseToken, signOut } from '../helpers/auth';
import { addTimedNotification, clearNotifications } from './notifications';
import { change, untouch } from 'redux-form';
import { browserHistory } from 'react-router';
import { REHYDRATE } from 'redux-persist/constants'

const UNAUTH_USER = 'UNAUTH_USER'
const FETCHING_AUTH = 'FETCHING_AUTH'
const FETCHING_AUTH_SUCCESS = 'FETCHING_AUTH_SUCCESS'
const FETCHING_AUTH_FAILURE = 'FETCHING_AUTH_FAILURE'


function unauthUser () {
    return {
        type: UNAUTH_USER
    }
}

function fetchingAuth () {
    return {
        type: FETCHING_AUTH
    }
}

function fetchingAuthFailure (error) {
    return {
        type: FETCHING_AUTH_FAILURE,
        error
    }
}

function fetchingAuthSuccess (auth) {
  return {
    type: FETCHING_AUTH_SUCCESS,
    auth
  }
}

const initialState = {
    isRehydrating: true,
    isFetching: false,
    error: '',
    isAuthed: false,
    idToken: '',
    accessToken: '',
    refreshToken: '',
    authDetails: {}
}

export default function authentication (state = initialState, action) {
    switch (action.type) {
        case REHYDRATE :
            let incoming = action.payload.authentication
            if (incoming) return {...state, ...incoming}
            return state
        case UNAUTH_USER :
            return {
                ...state,
                error: '',
                isAuthed: false,
                idToken: '',
                accessToken: '',
                refreshToken: '',
                authDetails: {}
            }
        case FETCHING_AUTH :
            return {
                ...state,
                isFetching: true
            }
        case FETCHING_AUTH_FAILURE :
         return {
             ...state,
             isFetching: false,
             error: action.error
         }
        case FETCHING_AUTH_SUCCESS :
            return {
                ...state,
                isFetching: false,
                error: '',
                isAuthed: true,
                idToken: action.auth.idToken.jwtToken,
                accessToken: action.auth.accessToken.jwtToken,
                refreshToken: action.auth.refreshToken.token,
                authDetails: parseToken(action.auth.idToken.jwtToken)
            }
        default :
            return state
    }
}

export function loginUser (username, password) {
    return function (dispatch) {
        dispatch(fetchingAuth())
        return login(username, password)
        .then((response) => {
            browserHistory.replace('/dashboard')
            dispatch(clearNotifications())
            dispatch(fetchingAuthSuccess(response))
            
        })
        .catch((error) => {
            dispatch(untouch('login', 'password'))
            dispatch(change('login', 'password', ''))  
            dispatch(fetchingAuthFailure(error.message))
            dispatch(addTimedNotification('light-red', error.message))
        })
    }
}

export function logoutUser (username) {
  return function (dispatch) {
    signOut(username)
    dispatch(unauthUser())
    dispatch(addTimedNotification('green', 'Successfully signed out.'))
    browserHistory.replace('/')
    browserHistory.push('/login')
  }
}