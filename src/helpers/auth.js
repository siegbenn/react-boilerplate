import { CognitoUserPool, CognitoUser, AuthenticationDetails } from 'amazon-cognito-identity-js';
import jwt from 'jwt-decode';
// CognitoUserAttribute

const poolData = {
    UserPoolId: 'us-east-1_yVLNZvxTY',
    ClientId: '7m530oqstl7t7ufthi1d9nr2eb'
};

const userPool = new CognitoUserPool(poolData);

export function signUp(username, email, password) {

    let attributeList = [];

    let dataEmail = {
        Name: 'email',
        Value: email
    }

    attributeList.push(dataEmail);

    return new Promise(function(fulfill, reject) {
        userPool.signUp(username, password, attributeList, null, function(error, data) {
            if (error) reject(error)
            else fulfill(data)
        })
    })
}

export function verify(username, code) {

    let userData = {
        Username: username,
        Pool: userPool
    }

    let cognitoUser = new CognitoUser(userData)

    return new Promise(function(fulfill, reject) {
        cognitoUser.confirmRegistration(code, true, function(error, data) {
            if (error) reject(error)
            else fulfill(data)
        })
    })
}

export function login(username, password) {

    let authenticationData = {
        Username: username,
        Password: password,
    };

    var authenticationDetails = new AuthenticationDetails(authenticationData);
    var userData = {
        Username: username,
        Pool: userPool
    };
    var cognitoUser = new CognitoUser(userData);

    return new Promise(function(fulfill, reject) {
        cognitoUser.authenticateUser(authenticationDetails, {
            onSuccess: function(result) {
                fulfill(result)
            },

            onFailure: function(error) {
                reject(error)
            },
        });
    })
}

export function signOut(username) {

    var userData = {
        Username: username,
        Pool: userPool
    };
    var cognitoUser = new CognitoUser(userData);
    cognitoUser.signOut();

    return true;

}

export function recover(username) {

    var userData = {
        Username: username,
        Pool: userPool
    };
    var cognitoUser = new CognitoUser(userData);
    return new Promise(function(fulfill, reject) {
        cognitoUser.forgotPassword({
            onSuccess: function(result) {
                fulfill(result)
            },

            onFailure: function(error) {
                reject(error)
            },
        });
    })

}

export function resetPassword(username, code, password) {

    var userData = {
        Username: username,
        Pool: userPool
    };
    var cognitoUser = new CognitoUser(userData);
    return new Promise(function(fulfill, reject) {
        cognitoUser.confirmPassword(code, password, {
            onSuccess: function(result) {
                fulfill(result)
            },

            onFailure: function(error) {
                reject(error)
            },
        });
    })

}

export function parseToken (token) {
	return jwt(token)
}
