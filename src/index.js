// React
import React from 'react'
import ReactDOM from 'react-dom'

// Redux
import * as reducers from './redux'
import { createStore, compose, combineReducers, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'
import { reducer as formReducer } from 'redux-form'
import { persistStore, autoRehydrate } from 'redux-persist'

// Routing
import getRoutes from './config/routes'
import { browserHistory } from 'react-router'
import { routerReducer, syncHistoryWithStore } from 'react-router-redux'

// Initialize Redux store with middleware.
const store = createStore(combineReducers({
    ...reducers,
    routing: routerReducer,
    form: formReducer
}), compose(
    applyMiddleware(thunk),
    autoRehydrate(),
    window.devToolsExtension ? window.devToolsExtension() : (f) => f
))

// Sync browser history with Redux store.
const history = syncHistoryWithStore(browserHistory, store)

// Restrict routes.
function checkAuth(nextState, replace) {
    let { isAuthed, isFetching } = store.getState().authentication
    if (isFetching === true) {
        return
    }

    const nextPathName = nextState.location.pathname

    if (['/logout', '/dashboard'].indexOf(nextPathName) !== -1) {
        if (isAuthed !== true) {
            replace('/login')
        }
    }

    if (['/', '/login', '/register', '/verify', '/recover'].indexOf(nextPathName) !== -1) {
        if (isAuthed === true) {
            replace('/dashboard')
        }
    }
}

persistStore(store, { blacklist: ["notifications", "routing"] }, () => {
    ReactDOM.render(
        <Provider store={store}>
          {getRoutes(history, checkAuth)}
        </Provider>,
        document.getElementById('root')
      )
})