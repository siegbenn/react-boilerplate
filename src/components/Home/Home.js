import React from 'react';

const Notifications = (props) => {

    return (
    	<div className="tc white">
	        <div className="bg-purple tc pv6 ph3">
	    		<h1>Section 1</h1>
	      		<p>section 1</p>
			</div>
			<div className="bg-washed-green pv6 ph3">
	    		<h1>Section 2</h1>
	      		<p>section 2</p>
			</div>
			<div className="bg-washed-red pv6 ph3">
	    		<h1>Section 2</h1>
	      		<p>section 2</p>
			</div>
		</div>
    )
}

export default Notifications;
