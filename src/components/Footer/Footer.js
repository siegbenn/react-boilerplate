import React, { Component } from 'react';

class Footer extends Component {

    render() {
        return (
            <footer className="f4-ns f5-m f5 pv3 ph1 white bg-black-80">
             <small className="db tc">© Copyright 2017, GMSW, Inc. All rights reserved.</small>
              <div className="tc mt2">
              <a href="/language/" title="Language" className="f6 dib ph2 link white dim">Language</a>
              <a href="/terms/"    title="Terms" className="f6 dib ph2 link white dim">Terms of Use</a>
              <a href="/privacy/"  title="Privacy" className="f6 dib ph2 link white dim">Privacy</a>
            </div>
          </footer>
        )
    };
}

export default Footer;
