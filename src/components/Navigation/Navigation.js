import React, { Component } from 'react';
import { Link } from 'react-router';

class Navigation extends Component {

    render() {
        return (
            <nav className="f3-l f4-m f5 bg-light-blue">
			    <a className="link dim pv2 ph3 dib black bg-light-blue" href="#" title="Home">GMSW Boilerplate</a>
			    <div hidden={this.props.isAuthed} className="fr">
			    	<Link to={'/login'} className="link dim ph3 pv2 dib black bg-light-green">Log In</Link>
  					<Link to={'/register'} className="link dim ph3 pv2 dib black bg-yellow">Sign Up</Link>
  				</div>
  				<div hidden={!this.props.isAuthed} className="fr">
			    	<Link to={'/logout'} className="link dim ph3 pv2 dib black bg-washed-blue">Log Out</Link>
  				</div>
			</nav>
        )
    };
}


export default Navigation;
