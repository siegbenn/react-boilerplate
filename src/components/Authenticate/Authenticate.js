import React, { PropTypes } from 'react';

const Authenticate = ({ submit, updateUsername, updatePassword, username, password, rememberMe, changeRememberMe }) => {

    return (
        <form onSubmit={submit}>
            <input type="text" value={username} placeholder="Username" onChange={(e) => updateUsername(e.target.value)} autoFocus  />
            <input type="password" value={password} placeholder="Password" onChange={(e) => updatePassword(e.target.value)} />
            <input type="checkbox" checked={rememberMe} onChange={(e) => changeRememberMe()}/>
            <button type="submit">Log In</button>
        </form>
    )
}

Authenticate.propTypes = {
    isFetching: PropTypes.bool.isRequired,
    error: PropTypes.string.isRequired,
    submit: PropTypes.func.isRequired,
    updateUsername: PropTypes.func.isRequired,
    updatePassword: PropTypes.func.isRequired,
    username: PropTypes.string.isRequired,
    password: PropTypes.string.isRequired,
    rememberMe: PropTypes.bool.isRequired

}

export default Authenticate;
