import React from 'react';

const Notifications = (props) => {
    
    return (
        <div>
            {props.notifications.map(([key, notification]) => (
            <div key={key} className={"br3 pointer bg-" + notification.type} onClick={((e) => props.removeNotification(e, key))}>
                <p className="tc f4-ns f5 pa2  white">{notification.text}
                {
                //<span className="fr cf">
                    // <a className="pr pointer" aria-hidden="true" onClick={((e) => props.removeNotification(e, key))}>×</a>
                //</span>
                }
                </p>
            </div>
            ))}  
        </div>
    )
};

export default Notifications;