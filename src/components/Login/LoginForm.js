import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router';

const validate = values => {
    const errors = {}
    if (!values.username) {
        errors.username = 'Oops, we need a username.'
    }

    if (!values.password) {
        errors.password = 'Please enter your password.'
    }

    return errors
}


const renderField = ({ focus, label, input, placeholder, type, meta: { touched, error, warning } }) => (
  <div>
    <label htmlFor={label}>{label}</label><br />
    <input autoFocus={focus} className="w-100 measure mt1" {...input} placeholder={placeholder} type={type}/>
    {touched && ((error && <p className='bg-light-red pa2 br1 white'>{error}</p>))}
  </div>
)


class LoginForm extends Component {
    render() {
        const { handleSubmit, invalid, isFetching } = this.props
        return (
            <div className="mt6-l mt5-m mt4">
                <h1 className="f2-ns f3">Login</h1><hr />
                <form onSubmit={handleSubmit} className="pt3 f4-ns f5">
                        <Field focus="true" label="Username" name="username" component={renderField}  type="text" placeholder="starlord1" /><br />
                        <Field label="Password" name="password" component={renderField}  type="password"  placeholder="&#8226;&#8226;&#8226;&#8226;&#8226;&#8226;&#8226;" /><br />
                        <div>
                            {invalid || isFetching
                                ? <input disabled className="ph3 pv2 ba br3 b--gray bg-gray white" type="submit" value="Log In" />
                                : <input className="ph3 pv2 dim ba br3 b--green pointer bg-green white" type="submit" value="Log In" />
                            }
                            { isFetching ? <div className="cf fr mt2 loader"></div> : null }
                        </div>
                </form>
                <p>Forgot your password? <Link to={'/recover'} className="">Reset it</Link>.</p>
                </div>
        )
    }
}

LoginForm = reduxForm({
    form: 'login',
    validate
})(LoginForm);

export default LoginForm;