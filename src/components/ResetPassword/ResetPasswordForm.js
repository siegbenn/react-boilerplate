import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';

const validate = values => {
    const errors = {}
    if (!values.username) {
        errors.username = 'Oops, we need a username.'
    }

    return errors
}


const renderField = ({ focus, label, input, placeholder, type, meta: { touched, error, warning } }) => (
  <div>
    <label htmlFor={label}>{label}</label><br />
    <input autoFocus={focus} className="w-100 measure mt1" {...input} placeholder={placeholder} type={type}/>
    {touched && ((error && <p className='bg-light-red pa2 br1 white'>{error}</p>))}
  </div>
)


class ResetPasswordForm extends Component {
    render() {
        const { handleSubmit, invalid, isFetching } = this.props
        return (
            <div className="mt6-l mt5-m mt4">
                <h1 className="f2-ns f3">Set New Password</h1><hr />
                <form onSubmit={handleSubmit} className="pt3 f4-ns f5">
                        <Field focus="true" label="Username" name="username" component={renderField}  type="text" placeholder="starlord1" /><br />
                        <Field label="Verification Code" name="code" component={renderField}  type="text" placeholder="184549" /><br />
                        <Field label="New Password" name="password" component={renderField}  type="password" placeholder="&#8226;&#8226;&#8226;&#8226;&#8226;&#8226;&#8226;" /><br />
                        <div>
                            {invalid || isFetching
                                ? <input disabled className="ph3 pv2 ba br3 b--gray bg-gray white" type="submit" value="Set New Password" />
                                : <input className="ph3 pv2 dim ba br3 b--green pointer bg-green white" type="submit" value="Set New Password" />
                            }
                            { isFetching ? <div className="cf fr mt2 loader"></div> : null }
                        </div>
                            
                </form>
                </div>
        )
    }
}

ResetPasswordForm = reduxForm({
    form: 'resetPassword',
    validate
})(ResetPasswordForm);

export default ResetPasswordForm;