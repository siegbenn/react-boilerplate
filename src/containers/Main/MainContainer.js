import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NotificationsContainer, NavigationContainer } from '..';
import { Footer } from '../../components';

class MainContainer extends Component {
    render() {

    	if (['/register', '/verify', '/login', '/recover', '/reset'].indexOf(this.props.pathname) === -1) {

    		return (
    			<div className="vh-100 bg-light-gray">
    				<div className="mw8-ns center">
    					<NavigationContainer />
    					<NotificationsContainer />
               			{this.props.children}
    					<Footer />
    				</div>
    			</div>

    		)
    	} else {
    		return (
    			<div className="vh-100 bg-washed-yellow">
    				<div className="mw8-ns center pt3 pt4-ns ph3 ph4-ns">
    				<NotificationsContainer />
    				</div>
    				<div className="mw6-ns center pa3 pa4-ns">
	               		{this.props.children}
	               	<Footer />
	               	</div>
    			</div>
    		)
    	}
    }
}

function mapStateToProps({ routing }) {
    return {
        pathname: routing.locationBeforeTransitions.pathname
    }
}

export default connect(
    mapStateToProps
)(MainContainer)
