import React, { Component, PropTypes } from 'react';
import { RecoverForm } from '../../components';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { recoverUser } from '../../redux/registration';

class RecoverContainer extends Component {

	handleSubmit = (e) => {
    // Do something with the form values
    	e.preventDefault();
    	this.props.recoverUser(
    		this.props.recoverForm.values.username
        )
  	}

    render() {
        return (
            <div>
            	<RecoverForm handleSubmit={this.handleSubmit} isFetching={this.props.isFetchingRecover} />
            </div>
        )
    };
}

RecoverContainer.propTypes = {
    recoverUser: PropTypes.func.isRequired
}

function mapStateToProps({ form, registration }) {
    return {
    	recoverForm: form.recover,
    	isFetchingRecover: registration.isFetchingRecover

    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ recoverUser }, dispatch)
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RecoverContainer)
