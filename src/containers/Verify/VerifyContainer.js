import React, { Component, PropTypes } from 'react';
import { VerifyForm } from '../../components';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { verifyUser } from '../../redux/registration';

class VerifyContainer extends Component {

    handleSubmit = (e) => {
    // Do something with the form values
        e.preventDefault();
        this.props.verifyUser(
            this.props.verifyForm.values.username,
            this.props.verifyForm.values.code)
    }

    render() {
        return (
            <div>
                <VerifyForm handleSubmit={this.handleSubmit} isFetching={this.props.isFetchingVerification} />
            </div>
        )
    };
}

VerifyContainer.propTypes = {
    verifyUser: PropTypes.func.isRequired
}

function mapStateToProps({ form, registration }) {
    return {
        verifyForm: form.verify,
        isFetchingVerification: registration.isFetchingVerification

    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ verifyUser }, dispatch)
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(VerifyContainer)
