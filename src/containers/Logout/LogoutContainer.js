import { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { logoutUser } from '../../redux/authentication';

class LogoutContainer extends Component {
    componentDidMount() {
        this.props.logoutUser(this.props.username)
    }

    render() {
        return null
    }
}

LogoutContainer.propTypes = {
    logoutUser: PropTypes.func.isRequired
}

function mapStateToProps({ form, authentication }) {
    return {
        username: authentication.authDetails["cognito:username"]

    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ logoutUser }, dispatch)
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(LogoutContainer)
