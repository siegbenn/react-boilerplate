import React, { Component, PropTypes } from 'react';
import { LoginForm } from '../../components';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { loginUser } from '../../redux/authentication';

class LoginContainer extends Component {

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.loginUser(
            this.props.loginForm.values.username,
            this.props.loginForm.values.password
        )
    }
    render() {
        return (
            <div>
                <LoginForm handleSubmit={this.handleSubmit} isFetching={this.props.isFetching}/>
            </div>
        )
    };
}

LoginContainer.propTypes = {
    loginUser: PropTypes.func.isRequired
}

function mapStateToProps({ form, authentication }) {
    return {
        loginForm: form.login,
        isFetching: authentication.isFetching
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ loginUser }, dispatch)
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(LoginContainer)
