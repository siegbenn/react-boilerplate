import React, { Component, PropTypes } from 'react';
import { RegisterForm } from '../../components';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as registrationActionCreators from '../../redux/registration';

class RegisterContainer extends Component {

	handleSubmit = (e) => {
    // Do something with the form values
    	e.preventDefault();
    	this.props.signUpUser(
    		this.props.registerForm.values.username,
    		this.props.registerForm.values.email,
    		this.props.registerForm.values.password)
  	}

    render() {
        return (
            <div>
            	<RegisterForm handleSubmit={this.handleSubmit} isFetching={this.props.isFetchingRegistration} />
            </div>
        )
    };
}

RegisterContainer.propTypes = {
    signUpUser: PropTypes.func.isRequired
}

function mapStateToProps({ form, registration }) {
    return {
    	registerForm: form.register,
    	isFetchingRegistration: registration.isFetchingRegistration

    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(registrationActionCreators, dispatch)
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RegisterContainer)
