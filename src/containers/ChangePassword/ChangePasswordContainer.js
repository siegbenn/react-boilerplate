import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { ChangePasswordForm } from '../../components'
import { bindActionCreators } from 'redux';
import { changePassword } from '../../redux/user';

class ChangePasswordContainer extends Component {

	handleSubmit = (e) => {
    	e.preventDefault();
    	this.props.changePassword(
    		this.props.changePasswordForm.values.password
        )
  	}

    render() {
        return (
            <div>
            	<ChangePasswordForm handleSubmit={this.handleSubmit} isFetching={this.props.isFetchingChangePassword} />
            </div>
        )
    };
}

ChangePasswordContainer.propTypes = {
    changePassword: PropTypes.func.isRequired
}

function mapStateToProps({ form, user }) {
    return {
    	changePasswordForm: form.changePassword,
    	isFetchingChangePassword: user.isFetchingChangePassword

    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ changePassword }, dispatch)
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ChangePasswordContainer)
