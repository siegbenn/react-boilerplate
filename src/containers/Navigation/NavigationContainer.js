import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Navigation } from '../../components';

class NavigationContainer extends Component {
    render() {
        return (
            <Navigation isAuthed={this.props.isAuthed} />
        )
    }
}

function mapStateToProps({ authentication }) {
    return {
        isAuthed: authentication.isAuthed

    }
}

export default connect(
    mapStateToProps
)(NavigationContainer)