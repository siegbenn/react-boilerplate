import React, { Component, PropTypes } from 'react';
import { Notifications } from '../../components';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as notificationsActionCreators from '../../redux/notifications';

class NotificationsContainer extends Component {

    addNotification = ((e, type, text) => {
        e.preventDefault()
        this.props.addTimedNotification(type, text)
    })

    removeNotification = ((e, id) => {
        e.preventDefault()
        this.props.removeNotification(id)
    })

    render() {
        const notificationsSeq = this.props.notifications.entrySeq()
        return (
            <Notifications notifications={notificationsSeq} addNotification={this.addNotification} removeNotification={this.removeNotification}/>
        );
    }
}

NotificationsContainer.propTypes = {
    notifications: PropTypes.object.isRequired,
    addTimedNotification: PropTypes.func.isRequired,
    removeNotification: PropTypes.func.isRequired
}

function mapStateToProps({ notifications }) {
    return {
        notifications: notifications.get('notifications')
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(notificationsActionCreators, dispatch)
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(NotificationsContainer)
