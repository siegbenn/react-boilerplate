import React, { Component, PropTypes } from 'react';
import { ResetPasswordForm } from '../../components';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { resetUserPassword } from '../../redux/registration';

class ResetPasswordContainer extends Component {

	handleSubmit = (e) => {
    // Do something with the form values
    	e.preventDefault();
    	this.props.resetUserPassword(
    		this.props.resetPasswordForm.values.username,
            this.props.resetPasswordForm.values.code,
            this.props.resetPasswordForm.values.password

        )
  	}

    render() {
        return (
            <div>
            	<ResetPasswordForm handleSubmit={this.handleSubmit} isFetching={this.props.isFetchingResetPassword} />
            </div>
        )
    };
}

ResetPasswordContainer.propTypes = {
    resetUserPassword: PropTypes.func.isRequired
}

function mapStateToProps({ form, registration }) {
    return {
    	resetPasswordForm: form.resetPassword,
    	isFetchingResetPassword: registration.isFetchingResetPassword

    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ resetUserPassword }, dispatch)
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ResetPasswordContainer)
