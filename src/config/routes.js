import React from 'react'
import { Router, Route, IndexRoute } from 'react-router'
import {
    LoginContainer,
    LogoutContainer,
    HomeContainer,
    MainContainer,
    NotFoundContainer,
    RecoverContainer,
    RegisterContainer,
    ResetPasswordContainer,
    VerifyContainer
} from '../containers'

export default function getRoutes(history, checkAuth) {
    return (
        <Router history={history}>
            <Route path='/' component={MainContainer}>
                <IndexRoute component={HomeContainer} onEnter={checkAuth}/>
                <Route path='verify' component={VerifyContainer} onEnter={checkAuth}/>
                <Route path='recover' component={RecoverContainer} onEnter={checkAuth}/>
                <Route path='reset' component={ResetPasswordContainer} onEnter={checkAuth}/>
                <Route path='login' component={LoginContainer} onEnter={checkAuth}/>
                <Route path='dashboard' component={HomeContainer} onEnter={checkAuth}/>
                <Route path='register' component={RegisterContainer} onEnter={checkAuth}/>
                <Route path='logout' component={LogoutContainer} onEnter={checkAuth}/>
            </Route>
            <Route path='*' component={NotFoundContainer} />
        </Router>
    )
}
